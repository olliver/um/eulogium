/*
 * function and data types for dbus calls
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _DBUS_COMMON_H
#define _DBUS_COMMON_H

#include <Eina.h>
#include <Eldbus.h>
#include <stdint.h>

struct dbus_handle {
	const char *signal;
	Eldbus_Signal_Cb sig_cb;
	const char *get;
	Eldbus_Message_Cb get_cb;
	const char *set;
	Eldbus_Message_Cb set_cb;
	void *set_data;
};

void on_method_generic_ret(void *data EINA_UNUSED, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED);
void on_method_generic_bool_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED);
void on_method_get_network_info_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED);
void on_signal_network_info_changed_ret(void *data, const Eldbus_Message *msg);
void on_method_is_network_powered_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED);
void on_signal_network_power_changed_ret(void *data, const Eldbus_Message *msg);
void on_method_get_procedure_metadata_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED);
void on_signal_procedure_metadata_changed_ret(void *data, const Eldbus_Message *msg);

#endif /* _DBUS_COMMON_H */
