/*
 * eulogium_item_list, inspired elm_list.
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _EULOGIUM_ITEM_LIST_H
#define _EULOGIUM_ITEM_LIST_H

#include <Elementary.h>

enum item_type {
	LIST_ITEM_BUTTON,
	LIST_ITEM_CHECK,
	LIST_ITEM_RADIO,
};

struct list_item {
	enum item_type type;
	union {
		Eina_Bool state;
		int radio_state;
	};
	Evas_Object *_widget;
};

Evas_Object *eulogium_item_list_add(Evas_Object *parent);
Evas_Object *eulogium_item_list_prepend(Evas_Object *list, Evas_Object *icon,
					char const *label, struct list_item *item,
					Evas_Smart_Cb func, const void *data);
Evas_Object *eulogium_item_list_append(Evas_Object *list, Evas_Object *icon,
				       char const *label, struct list_item *item,
				       Evas_Smart_Cb func, const void *data);
Evas_Object *eulogium_item_list_sorted_insert(Evas_Object *list, Evas_Object *icon,
					      char const *label, struct list_item *item,
					      Evas_Smart_Cb func, const void *data,
					      Eina_Compare_Cb cmp_func);
void eulogium_item_list_go(Evas_Object *list);

#endif /* _EULOGIUM_ITEM_LIST_H */
