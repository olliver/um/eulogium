/*
 * function and data types for print information
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Eina.h>

#include "print_data.h"

void print_clear(struct print_data *print)
{
	print->jobname_changed = EINA_FALSE;
	free(print->jobname);
	print->jobname = NULL;

	print->time = 0;
	print->total_time = 0;
	print->progress = 0.0;
}
