/*
 * functions, callbacks and data types for user input
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _UI_INPUT_H
#define _UI_INPUT_H

extern int INPUT_MOUSE_WHEEL_UP;
extern int INPUT_MOUSE_WHEEL_DOWN;

void input_mouse_wheel_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info);
void input_init(void);

#endif /* _UI_INPUT_H */
