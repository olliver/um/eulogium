/*
 * Eulogium_item_list, inspired elm_list.
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Elementary.h>
#include <libintl.h>

#include "eulogium_item_list.h"
#include "eulogium_private.h"
#include "gettext.h"


Evas_Object *eulogium_item_list_add(Evas_Object *parent)
{
	Evas_Object *list, *box;
	Eina_List *l = NULL;

	list = elm_scroller_add(parent);
	evas_object_data_set(list, "eulogium_item_list", l);
	box = elm_box_add(list);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, 0);
	evas_object_data_set(list, "content", box);
	elm_object_content_set(list, box);
	evas_object_show(box);

	return list;
}

static void _item_destroy_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
	Evas_Object **_widget = data;

	*_widget = NULL;
}

static Evas_Object *_item_new(Evas_Object *parent, Evas_Object *icon, char const *label,
			      struct list_item *item,/* Evas_Object *radio_group, */ Evas_Smart_Cb func, const void *data)
{
	Evas_Object *it;
	const char *cb = "changed";
	struct list_item dummy = {.type = LIST_ITEM_BUTTON, ._widget = NULL};

	if (!item) /* If no button type is passed (file-list for ex.) use the default 'button' entry */
		item = &dummy;

	switch (item->type) {
	case LIST_ITEM_RADIO:
#if 0
		if (!radio_group)
			it = elm_radio_add(parent);
		else
			it = elm_radio_group_add(parent, radio_group);
		elm_radio_pointer_set(it, &item->radio_state);
		elm_object_style_set(it, "list");
#endif
		break;
	case LIST_ITEM_CHECK:
		it = elm_check_add(parent);
		elm_check_state_pointer_set(it, &item->state);
		/* elm_object_style_set(it, "list"); */
		/* XXX FIXME: this is a hack. PRINT -> Return -> Segfault.
		 * When we return to the from the file list in the PRINT menu, we segfault.
		 * Technically we don't want to store the pointer to the widget for anything
		 * but checkboxes and radio buttons, but a generic callback at the bottom for
		 * all cases was nicer. Since we don't have radio buttons yet we can leave
		 * this here for now.
		 */
		evas_object_event_callback_add(it, EVAS_CALLBACK_DEL, _item_destroy_cb, &item->_widget);
		if (evas_alloc_error() != EVAS_ALLOC_ERROR_NONE)
			EINA_LOG_CRIT("Callback registering failed! Problems ensured!\n");
		break;
	case LIST_ITEM_BUTTON:
		cb = "clicked";
		it = elm_button_add(parent);
		elm_object_style_set(it, "list");
		break;
	default:
		return NULL;
	}
	if (label)
		elm_object_text_set(it, _(label));
	if (icon)
		elm_object_part_content_set(it, "icon", icon);
	if (data)
		evas_object_data_set(it, "sort_data", data);
	if (func)
		evas_object_smart_callback_add(it, cb, func, data);
	evas_object_size_hint_weight_set(it, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(it, EVAS_HINT_FILL, 0);
	evas_object_show(it);
	item->_widget = it;

	return it;
}

Evas_Object *eulogium_item_list_prepend(Evas_Object *list, Evas_Object *icon,
					char const *label, struct list_item *item,
					Evas_Smart_Cb func, const void *data)
{
	Evas_Object *box = evas_object_data_get(list, "content");
	Eina_List *l = evas_object_data_get(list, "eulogium_item_list");
	Evas_Object *it;

	if ((!box) && (!list))
		return NULL;

	it = _item_new(box, icon, label, item, func, data);
	if (!it)
		return NULL;

	if (l)
		l = eina_list_prepend(l, it);
	else
		l = eina_list_prepend(NULL, it);
	evas_object_data_set(list, "eulogium_item_list", l);

	return it;
}

Evas_Object *eulogium_item_list_append(Evas_Object *list, Evas_Object *icon,
					char const *label, struct list_item *item,
					Evas_Smart_Cb func, const void *data)
{
	Evas_Object *box = evas_object_data_get(list, "content");
	Eina_List *l = evas_object_data_get(list, "eulogium_item_list");
	Evas_Object *it;

	if ((!box) && (!list))
		return NULL;

	it = _item_new(box, icon, label, item, func, data);
	if (!it)
		return NULL;

	if (l)
		l = eina_list_append(l, it);
	else
		l = eina_list_append(NULL, it);
	evas_object_data_set(list, "eulogium_item_list", l);

	return it;
}

Evas_Object *eulogium_item_list_sorted_insert(Evas_Object *list, Evas_Object *icon,
					      char const *label, struct list_item *item,
					      Evas_Smart_Cb func, const void *data,
					      Eina_Compare_Cb cmp_func)
{
	Evas_Object *box = evas_object_data_get(list, "content");
	Eina_List *l = evas_object_data_get(list, "eulogium_item_list");
	Evas_Object *it;

	if ((!box) && (!list))
		return NULL;

	it = _item_new(box, icon, label, item, func, data);
	if (!it)
		return NULL;

	if (l)
		l = eina_list_sorted_insert(l, cmp_func, it);
	else
		l = eina_list_sorted_insert(NULL, cmp_func, it);
	evas_object_data_set(list, "eulogium_item_list", l);

	return it;
}

void eulogium_item_list_go(Evas_Object *list)
{
	Evas_Object *box = evas_object_data_get(list, "content");
	Eina_List *l, *item_list = evas_object_data_get(list, "eulogium_item_list");
	Evas_Object *it;

	if ((!box) && (!list))
		return;

	EINA_LIST_FOREACH(item_list, l, it)
		elm_box_pack_end(box, it);
}
