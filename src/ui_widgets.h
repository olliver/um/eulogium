/*
 * functions, callbacks and data types for widgets
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _UI_WIDGETS_H
#define _UI_WIDGETS_H

#include <Ecore.h>
#include <Eina.h>
#include <Evas.h>
#include <stdint.h>

#include "dbus_common.h"
#include "eulogium_item_list.h"
#include "settings_data.h"

#define DIAL_MAX_FORMAT_SIZE 10 /* Maximally allowed characters on a dial, inc comma and \0 */

struct button_cb {
	void (*func)(void *data, Evas_Object *object, void *event_info);
	void *data;
	const char *info;
};

struct button_def {
	const char *label;
	const char *text; /* TODO remove */
	struct button_cb cb;
	void *data;
};

struct dir_entry_def {
	void *data;
	char *path;
};

struct menu_entry_def {
	const char *icon;
	const char *label;
	struct list_item item;
	struct dbus_handle dbus;
	Eina_Bool *end; /* remove me */
	const char *footer; /* Primary footer */
	const char *footer_alt; /* Alternating footer */
	Ecore_Timer *toggle_timer; /*TODO, this may better go one level up into the menu */
	void (*func)(void *data, Evas_Object *object, void *event_info);
	void *data;
};

enum list_type {
	LIST_MENU,
	LIST_FILE,
	LIST_WIFI,
};

struct menu_def {
	const char *title;
	enum list_type type;
	void *data;
	struct dir_entry_def dir; /* TODO check if we need this here at all */
	struct menu_entry_def entry[];
};

struct screen_def {
	const char *title;
	Evas_Object *widget;
};

/* WARNING! This enum is stored in a void * via an (uintptr_t) typecast.
 * Always make sure that it fits into that. At birth, this enum was intended
 * to never have more then a handful of states.
 */
enum navi_page_state {
	PAGE_NORMAL,
	PAGE_BOTTOM,
	PAGE_PRINTING,
	PAGE_INVALID,
};

void eulogium_button_cb_set(struct button_def *button, struct button_cb *cb);
void eulogium_button_cb_data_set(struct button_def *button, void *data);
void eulogium_button_data_set(struct button_def *button, void *data);

struct eulogium_data; /* XXX remove when we pass a navi */
Elm_Object_Item *ui_stack_push(const struct eulogium_data *eulogium, Evas_Object *content, const enum navi_page_state page_state);
Eina_Bool ui_stack_pop(const struct eulogium_data *eulogium);
void ui_stack_pop_cb(void *data, Evas_Object *eo EINA_UNUSED, void *event_info EINA_UNUSED);
Evas_Object *ui_widget_dial(Evas_Object *parent, struct eulogium_data *eulogium, struct settings_dial_data *dial_data);
Evas_Object *ui_widget_rotator(Evas_Object *parent, struct eulogium_data *eulogium, struct settings_dial_data *dial_data);

#endif /* _UI_WIDGETS_H */
