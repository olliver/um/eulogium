/*
 * functions, callbacks and data types for user input
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Ecore.h>
#include <Elementary.h>
#include <Evas.h>

#include "ui_input.h"

int INPUT_MOUSE_WHEEL_UP = 0; /* For custom ecore_event handling */
int INPUT_MOUSE_WHEEL_DOWN = 0; /* For custom ecore_event handling */

/* Function to make the Z-axis of any wheel focus the next/previous focusable widget */
void input_mouse_wheel_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
	Evas_Event_Mouse_Wheel *ev = event_info;
	Ecore_Event *event = NULL;

	if (ev->z > 0) {
		elm_object_focus_next((Evas_Object *)data, ELM_FOCUS_NEXT);
		event = ecore_event_add(INPUT_MOUSE_WHEEL_DOWN, NULL, NULL, NULL);
	} else {
		elm_object_focus_next((Evas_Object *)data, ELM_FOCUS_PREVIOUS);
		event = ecore_event_add(INPUT_MOUSE_WHEEL_UP, NULL, NULL, NULL);
	}
	if (!event)
		EINA_LOG_ERR("Wheel event failed to queue.");
}

void input_init()
{
	INPUT_MOUSE_WHEEL_UP = ecore_event_type_new();
	INPUT_MOUSE_WHEEL_DOWN = ecore_event_type_new();
}
