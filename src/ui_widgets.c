/*
 * functions, callbacks and data types for widgets
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Elementary.h>
#include <stdarg.h>
#include <stdint.h>

#include "eulogium.h"
#include "eulogium_private.h"
#include "ui_input.h"
#include "ui_widgets.h"

Elm_Object_Item *ui_stack_push(const struct eulogium_data *eulogium, Evas_Object *content, const enum navi_page_state page_state) /* TODO replace with just the navi pointer */
{
	Elm_Object_Item *navi_item;
	if ((!eulogium->navi) || (!content)) {
		EINA_LOG_CRIT("navi or content where NULL");
		return NULL;
	}

	navi_item = elm_naviframe_item_simple_push(eulogium->navi, content);
	if (!navi_item) {
		EINA_LOG_CRIT("Unable to push item onto navistack");
		return NULL;
	}

	elm_object_item_data_set(navi_item, (void *)(uintptr_t)page_state);

	return navi_item;
}

/* Search through the navistack in reverse order. This loop is almost always
 * aborted very early on. We basically 'abuse' the loop to skip over invalid
 * pages, which is actually unlikely and thus the first item of the stack
 * (the tail) gets popped immediately. Additionally there is a safeguard in
 * place to not pop the very first page of the stack, as that is our
 * 'main-menu'. TODO Improve doc, this is just an early note.
 */
Eina_Bool ui_stack_pop(const struct eulogium_data *eulogium) /* TODO replace with just the navi pointer */
{
	Eina_List *navi_list, *l;
	Elm_Object_Item *navi_item;
	enum navi_page_state page_state;

	if (!eulogium->navi) {
		EINA_LOG_CRIT("Naviframe was NULL");
		return EINA_FALSE;
	}

	/* FIXME: bug here, when we start eulogium and start a print job,
	 * eulogium restarts midway the print job, the dialog with the
	 * print cleaned pops up, but apparently no bottom has been generated
	 * yet. Make sure to call eulogium_main_menu (new name) first before
	 * process handling. Call moved, but we may still have init issues.
	 */
	navi_item = elm_naviframe_top_item_get(eulogium->navi);
	page_state = (enum navi_page_state)(uintptr_t)elm_object_item_data_get(navi_item);
	if (page_state == PAGE_BOTTOM) {
		EINA_LOG_WARN("Not popping bottom frame cowboy");
		return EINA_TRUE;
	}

	navi_list = elm_naviframe_items_get(eulogium->navi);
	l = eina_list_last(navi_list);
	l = eina_list_prev(l);
	/* Skip the top entry, it is the currently displayed window and thus
	 * always valid, we also know the last item is not the bottom of the
	 * stack. Because we are looking 1 item ahead, if l becomes NULL, we
	 * must be on the bottom page, which may never be invalid by
	 * definition. */
	for (; l; l = eina_list_prev(l)) {
		navi_item = eina_list_data_get(l);
		if (!navi_item) {
			EINA_LOG_ERR("No item in list?!");
			continue;
		}
		page_state = (enum navi_page_state)(uintptr_t)elm_object_item_data_get(navi_item);
		if (page_state == PAGE_INVALID) {
			EINA_LOG_WARN("Page on stack invalid, skipping");
			continue;
		}
		/* All other page_states, PAGE_NORMAL, PAGE_PRINTING */
		elm_naviframe_item_pop_to(navi_item);
		eina_list_free(navi_list);
		return EINA_TRUE;

	}
	eina_list_free(navi_list);

	return EINA_FALSE;
}

void _event_handler_from_navi_del(Evas_Object *navi, const char *str)
{
	Ecore_Event_Handler *event;

	event = evas_object_data_get(navi, str);
	if (event)
		ecore_event_handler_del(event);
}

void ui_stack_pop_cb(void *data, Evas_Object *eo EINA_UNUSED, void *event_info EINA_UNUSED)
{
	struct eulogium_data *eulogium = data; /* XXX replace this with passing just the navi and event handlers */
	Evas_Object *navi = eulogium->navi;

	_event_handler_from_navi_del(navi, "event_inc"); /* TODO possibly use an array of event handlers if we get more then 3 */
	_event_handler_from_navi_del(navi, "event_dec"); /* TODO possibly use an array of event handlers if we get more then 3 */

	ui_stack_pop(eulogium); /* XXX replace with just the navi */
}

static void _dial_send_update(void *data, Evas_Object *eo, void *event_info EINA_UNUSED)
{
	struct settings_dial_data *dial_data = data;

	dial_data->value = elm_slider_value_get(eo);
	if (dial_data->method_set)
		dial_data->method_set(dial_data->proc_key);
}

static Eina_Bool _dial_change_delay(void *data)
{
	Evas_Object *dial = data;

	evas_object_data_set(dial, "delay", NULL);
	evas_object_smart_callback_call(dial, "delay,changed", NULL);

	return ECORE_CALLBACK_CANCEL;
}

static Eina_Bool _rotator_change(void *data, int type, void *event_info EINA_UNUSED)
{
	struct settings_dial_data *dial_data = data;

	/* TODO we now jus tell the backend that there is a change of + or - 1 * step size.
	 * ideally the event_info is filled with the step size/speed of the mouse_wheel events
	 * giving us the step size directly.
	 */
	//value = elm_slider_value_get(dial);
	//step = elm_slider_step_get(dial);
	if (type == INPUT_MOUSE_WHEEL_UP)
		dial_data->value = -1.0 * dial_data->step;
	if (type == INPUT_MOUSE_WHEEL_DOWN)
		dial_data->value = +1.0 * dial_data->step;
	if (dial_data->method_set)
		dial_data->method_set(dial_data->proc_key);

	return ECORE_CALLBACK_PASS_ON;
}

static Eina_Bool _dial_change(void *data, int type, void *event_info EINA_UNUSED)
{
	Evas_Object *dial = data;
	Ecore_Timer *delay;
	double value, step;

/* TODO XXX we call this dirty little hack because debian's v1.8 of elm does not offer us to actually call drag,left
 * evas_object_smart_callback_call(dial, "drag,left", NULL); would have been much cleaner and we didn't need to do
 * the val+step get/set thing and thus also benefit from delay,changed which we now 'fake' badly. Also a common user,changed
 * signal handler on the other end would help on making it less ugly.
 */
	value = elm_slider_value_get(dial);
	step = elm_slider_step_get(dial);
	if (type == INPUT_MOUSE_WHEEL_UP)
		elm_slider_value_set(dial, value - step);
	if (type == INPUT_MOUSE_WHEEL_DOWN)
		elm_slider_value_set(dial, value + step);
	evas_object_smart_callback_call(dial, "changed", NULL);
	delay = evas_object_data_get(dial, "delay");
	ecore_timer_del(delay);
	delay = ecore_timer_add(0.25, _dial_change_delay, dial); /* TODO make define for delay */
	evas_object_data_set(dial, "delay", delay);

	return ECORE_CALLBACK_PASS_ON;
}

/* HACK for some reason elm_slider_units_format_function_set does not supply us with any other variables
 * other then the value. Doing anything fancy thus becomes impossible. We therefore create these local
 * pointers which we set whenever a dial screen is generated. This brings us to the big caveat. Right
 * now, we only support one single active dial (unit/format) because of this XXX */
static const char *__dial_units_format = NULL;

static char *_dial_units_format(double val)
{
	char *buf;
	uint_fast32_t buf_size;

	buf_size = DIAL_MAX_FORMAT_SIZE;
	buf = malloc(buf_size);
	snprintf(buf, buf_size, __dial_units_format, val);

	return buf;
}

static void _dial_units_format_free(char *buf)
{
	free(buf);
}

static void _dial_units_end_object_text_set(Evas_Object *end, struct settings_dial_data *dial_data)
{
	char *buf;

	buf = malloc(DIAL_MAX_FORMAT_SIZE * sizeof(char));
	if (dial_data->value_end < 0)
		snprintf(buf, DIAL_MAX_FORMAT_SIZE, "%s", dial_data->unit);
	else
		snprintf(buf, DIAL_MAX_FORMAT_SIZE, dial_data->format_end, dial_data->value_end, dial_data->unit);
	elm_object_text_set(end, buf);
	free(buf);
}

static Eina_Bool _timer_dial_units_update(void *data)
{
	Evas_Object *dial = data;
	struct settings_dial_data *dial_data;

	dial_data = evas_object_data_get(dial, "dial_data");
	if (dial_data->proc_key > PROC_NONE)
		procedure_metadata_get(dial_data->proc_key);
	_dial_units_end_object_text_set(dial, dial_data);

	return ECORE_CALLBACK_RENEW;
}

static void _timer_del(void *data, Evas *e EINA_UNUSED, Evas_Object *eo EINA_UNUSED, void *event_info EINA_UNUSED)
{
	Ecore_Timer *timer = data;

	if (timer)
		ecore_timer_del(timer);
}

Evas_Object *ui_widget_rotator(Evas_Object *parent, struct eulogium_data *eulogium, struct settings_dial_data *dial_data)
{
	Evas_Object *_top, *_bottom;
	Ecore_Event_Handler *handler;

	_top = elm_label_add(parent);
	elm_object_text_set(_top, _(dial_data->label));
	evas_object_size_hint_weight_set(_top, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(_top, EVAS_HINT_FILL, EVAS_HINT_FILL);
	handler = ecore_event_handler_add(INPUT_MOUSE_WHEEL_UP, _rotator_change, dial_data);
	evas_object_data_set(parent, "event_inc", handler);
	handler = ecore_event_handler_add(INPUT_MOUSE_WHEEL_DOWN, _rotator_change, dial_data);
	evas_object_data_set(parent, "event_dec", handler);
	evas_object_show(_top);

	_bottom = elm_button_add(parent);
	elm_object_text_set(_bottom, _("Click when done")); /* TODO, make define for this text */
	evas_object_size_hint_weight_set(_bottom, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(_bottom, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_smart_callback_add(_bottom, "clicked", ui_stack_pop_cb, eulogium);
	evas_object_show(_bottom);

	return eulogium_split_screen(parent, _top, _bottom);
}

Evas_Object *ui_widget_dial(Evas_Object *parent, struct eulogium_data *eulogium, struct settings_dial_data *dial_data)
{
	Evas_Object *_top, *_bottom, *obj;
	Ecore_Timer *timer_value_end = NULL;
	Ecore_Event_Handler *handler;

	_top = elm_slider_add(parent);
	elm_object_text_set(_top, _(dial_data->label));
	elm_object_focus_allow_set(_top, EINA_FALSE);
	__dial_units_format = dial_data->format;
	elm_slider_units_format_function_set(_top, _dial_units_format, _dial_units_format_free);
	elm_slider_min_max_set(_top, dial_data->min, dial_data->max);
	elm_slider_step_set(_top, dial_data->step);
	elm_slider_value_set(_top, dial_data->value);
	evas_object_size_hint_weight_set(_top, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(_top, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_smart_callback_add(_top, "delay,changed", _dial_send_update, dial_data);
	handler = ecore_event_handler_add(INPUT_MOUSE_WHEEL_UP, _dial_change, _top);
	evas_object_data_set(parent, "event_inc", handler);
	handler = ecore_event_handler_add(INPUT_MOUSE_WHEEL_DOWN, _dial_change, _top);
	evas_object_data_set(parent, "event_dec", handler);
	evas_object_show(_top);

	/* if type is tar/cur do something different TODO */
	obj = elm_label_add(_top);
	evas_object_data_set(obj, "dial_data", dial_data);
	_dial_units_end_object_text_set(obj, dial_data);
	elm_object_part_content_set(_top, "end", obj);
	evas_object_show(obj);
	timer_value_end = ecore_timer_add(0.5, _timer_dial_units_update, obj); /* XXX replace with signal? */
	/* XXX also we only want to pass the obj (dial) to the screen update timer */
	evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, _timer_del, timer_value_end);

	_bottom = elm_button_add(parent);
	elm_object_text_set(_bottom, _("Click when done")); /* TODO, make define for this text */
	evas_object_size_hint_weight_set(_bottom, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(_bottom, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_smart_callback_add(_bottom, "clicked", ui_stack_pop_cb, eulogium);
	evas_object_show(_bottom);

	return eulogium_split_screen(parent, _top, _bottom);
}
