/*
 * function and data types for print information
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _PRINT_DATA_H
#define _PRINT_DATA_H

#include <Eina.h>
#include <Eldbus.h>
#include <stdint.h>

#include "procedures.h"
#include "settings_data.h"
#include "ui_widgets.h"

struct print_data {
	char *jobname;
	char *url;
	int_fast32_t time;
	int_fast32_t total_time;
	double progress;
	char *name; /* TODO deletion pending */
	char *flags;
	double material;
	Eina_Bool name_changed;
	Eina_Bool jobname_changed;
	Eina_Bool block;
	Eina_Bool block_active;
	Eldbus_Pending *(*method_set)(struct settings_dial_data *dial_data);
	Eldbus_Pending *(*method_get)(struct procedure_data *procedure);
	enum procedure_key proc_key;
};

enum printer_status {
	UNKNOWN,
	DISCONNECTED,
	ERROR,
	FIRST_RUN_WIZZARD,
	IDLE,
	SLICING,
	HEATING,
	PRINTING,
	COOLING,
	WAIT_FOR_REMOVAL,
	PAUSED,
};

void print_clear(struct print_data *print);

#endif /* _PRINT_DATA_H */
