/*
 * eulogium main header
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _EULOGIUM_H
#define _EULOGIUM_H

#include <Eeze_Disk.h>
#include <Eldbus.h>
#include <Evas.h>
#include <stdint.h>

#include "eulogium_item_list.h"
#include "network.h"
#include "print_data.h"
#include "procedures.h"
#include "settings_data.h"
#include "ui_widgets.h"

enum screen_type {
	NONE,
	FUNC,
	MATERIAL,
	PROGRESS,
	END, /* sentinel */
};

struct printer_data {
	enum printer_status status;
	char **file_handlers;
	uint_fast8_t hotend_count;
	double *hotend_temp;
	double *hotend_target_temp;
	double bed_temp;
	double bed_target_temp;
};

enum proxy_idx {
	PRINTER,
	NETWORK,
	LED,
	LAST /* sentinel */
};

struct dbus_data {
	Eldbus_Connection *conn;
	Eldbus_Pending *pending;
	Eldbus_Proxy *proxy[LAST];
};

struct mount_data {
	char *id;
	Eldbus_Proxy *proxy;
	Eeze_Disk *disk;
	Eina_List **mounts;
};

struct eulogium_data {
	Evas_Object *navi;
	Evas_Object *time;
	Evas_Object *name;
	Evas_Object *status;
	Evas_Object *progress;
	Ecore_Timer *progress_data_refresh;
	Evas_Object *footer; /* XXX this really shows why we need per window persistent data */
	Ecore_Event_Handler *event_inc; /* TODO  put in seperate event struct and generally find a better place (seperate file etc) */
	Ecore_Event_Handler *event_dec;
	struct printer_data printer;
	struct print_data print;
	struct dbus_data dbus;
	Eina_List *mounts;
	void *data;
	struct network_data *networks;
	struct procedure_data *procedures;
};

struct multi_text {
	uint_fast8_t count;
	const char *button_text;
	const char *text[];
};

struct multi_screen {
	enum screen_type type;
	char *text;
	void (*func)(void *data);
	void *data;
	char *prev_button;
	char *next_button;
	/* TODO consider adding a button_def for the buttons */
};

struct multi_screen_data {
	uint_fast8_t count;
	struct multi_screen *screen;
};

Evas_Object *eulogium_generic_error(struct eulogium_data *eulogium, int_fast16_t eulogium_error);

void eulogium_print_data_set(struct eulogium_data *eulogium, char *filepath);

Evas_Object *eulogium_main_menu(Evas_Object *window, struct eulogium_data *eulogium);
Evas_Object *eulogium_split_screen(Evas_Object *parent, Evas_Object *top, Evas_Object *bottom);
Evas_Object *eulogium_tripple_button_menu(Evas_Object *parent, const struct button_def *left, const struct button_def *right, const struct button_def *bottom);
Evas_Object *eulogium_dual_button_add(Evas_Object *parent, const struct button_def *left, const struct button_def *right);
Evas_Object *eulogium_print_menu(struct eulogium_data *eulogium, char *filepath);
Evas_Object *eulogium_print_ignore(struct eulogium_data *eulogium);
Evas_Object *eulogium_print_progress(Evas_Object *parent, struct eulogium_data *eulogium, const struct print_data *print);
Evas_Object *eulogium_pre_print(Evas_Object *parent, struct eulogium_data *eulogium);
Evas_Object *eulogium_settings_dial(Evas_Object *parent, struct eulogium_data *eulogium, struct settings_dial_data *dial_data);
Evas_Object *eulogium_multi_text_menu(struct eulogium_data *eulogium, Evas_Object *parent, struct button_def *button, struct multi_text *data, const uint_fast8_t pagenum, Eina_Bool pageindex);
Evas_Object *menu_widget_list(struct eulogium_data *eulogium, Evas_Object *parent, struct menu_def *menu);
Evas_Object *eulogium_multi_screen_menu(struct eulogium_data *eulogium, Evas_Object *parent, struct multi_screen_data *screen_data, const uint_fast8_t pagenum, Eina_Bool pageindex);
Evas_Object *eulogium_menu_confirm(Evas_Object *parent, const char *msg, const char *prev, const struct button_def *next);
Evas_Object *eulogium_clean_print_bed(struct eulogium_data *eulogium);
void eulogium_print_data_clear(struct eulogium_data *eulogium);

#endif /* _EULOGIUM_H */
