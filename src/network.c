/*
 * functions and data types for networking calls
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Eldbus.h>

#include "dbus_common.h"
#include "network.h"

#define _TECH_ETHERNET "ethernet"
#define _TECH_WIFI "wifi"

static Eldbus_Proxy *__proxy;

Eldbus_Pending *network_connect_wifi(const struct network_data *network)
{
	if (!network)
		return NULL;
	if (!network->obj_path)
		return NULL;

	return eldbus_proxy_call(__proxy, "connectWifiNetwork", on_method_generic_bool_ret, "connectWifiNetwork", -1, "o", network->obj_path);
}

void network_init(Eldbus_Proxy *proxy)
{
	__proxy = proxy;
};

void network_shutdown(void)
{
	eldbus_proxy_unref(__proxy);
};
