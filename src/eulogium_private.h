/*
 * eulogium_private, some common private helpers XXX Rename to something more appropiate
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _EULOGIUM_PRIVATE_H
#define _EULOGIUM_PRIVATE_H

#include <libintl.h>

#include "gettext.h"

#if HAVE_GETTEXT && ENABLE_NLS
#define _(string) gettext(string)
#define N_(string) gettext_noop(string)
#else
#define _(string) (string)
#endif

#endif /* _EULOGIUM_PRIVATE_H */
