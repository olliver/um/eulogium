/*
 * function and data types for settings information
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _SETTINGS_DATA_H
#define _SETTINGS_DATA_H

#include <Eldbus.h>

#include "procedures.h"

struct settings_dial_data {
	const char *label;
	double step;
	double min;
	double max;
	double value;
	double value_end;
	char *(*indicator_func)(double value);
	void (*free_func)(char *str);
	const char *format;
	const char *format_end;
	const char *unit;
	Eldbus_Pending *(*method_set)(enum procedure_key proc_key);
	Eldbus_Pending *(*method_get)(enum procedure_key proc_key);
	enum procedure_key proc_key;
};

#endif /* _SETTINGS_DATA_H */
