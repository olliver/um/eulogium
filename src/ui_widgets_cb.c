/*
 * widgets callbacks
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Eina.h>
#include <Elementary>
#include <Evas.h>

#include "ui_widgets.h"

void settings_dial_increase_cb(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
	
}

void settings_dial_decrease_cb(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
	
}
