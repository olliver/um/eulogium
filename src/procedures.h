/*
 * procedures, available procedures
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _PROCEDURES_H
#define _PROCEDURES_H

#include <Eina.h>
#include <Eldbus.h>
#include <stdint.h>

enum procedure_status {
	PROC_READY,
	PROC_ACTIVE,
	PROC_FINISHED,
};

enum procedure_key {
	PROC_NONE,
	PROC_HOTEND_LEVEL,
	PROC_PRINT,
	PROC_BED_HEATUP,
	PROC_HOTEND_HEATUP_0,
	PROC_HOTEND_HEATUP_1,
	PROC_HOTEND_ACTIVE_SET,
	PROC_PRINT_SPEED,
	PROC_FAN_SPEED,
	PROC_FLOW_RATE,
	PROC_RETRACTION_LENGTH,
	PROC_RETRACTION_SPEED,
	PROC_RETRACTION_ZHOP,
	PROC_BED_LEVEL_AUTO,
	PROC_BED_RAISE,
	PROC_BED_HOME,
	PROC_HEAD_HOME,
	PROC_MATERIAL_MOVE,
	PROC_JERK_XY,
	PROC_JERK_Z,
	PROC_POWER_XY,
	PROC_POWER_Z,
	PROC_POWER_E0,
	PROC_POWER_E1,
	PROC_LAST, /* sentinel */
};

enum steps {
	PROC_RUN_PRE_PRINT_SETUP,
	STEP_PRINTING,
	PROC_POST_PRINT,
	PROC_WAIT_FOR_CLEANUP,
	STEP_HEATING,
	STEP_MOVING,
	STEP_SPEEDING,
	STEP_FLOWING,
	STEP_HOMEING,
	STEP_LENGTHENING,
	STEP_HOPPING,
	STEP_RAISING,
	STEP_AUTOMATIC_BED_LEVELING,
	STEP_POWERING,
	STEP_JERKING,
	STEP_WAITING,
	STEP_SWITCHING,
};

struct procedure_step {
	const char *key;
	const enum steps step;
};

enum msgs {
	PROC_MSG_PRINTER_CLEANED,
};

struct procedure_msg {
	const char *key;
	const enum msgs msg;
};

enum meta_type {
	PROC_META_NONE,
	PROC_META_PRINT,
	PROC_META_DIAL,
};

struct procedure_data {
	const char *key;
	enum procedure_status status;
	const char *sig;
	const char *para; /* TODO va_list to go with sig? */
	Eina_Bool available;
	Eina_Bool executable;
	void (*parser)(void *data, const void *key, Eldbus_Message_Iter *variant);
	void *meta; /* TODO every proc. has meta data, some needs to be periodically updated (print_data), others on signal changes */
	const struct procedure_msg *msgs;
	const struct procedure_step *_steps; /* TODO optional? maybe we don't even have to know the list */
	struct procedure_step *step_active;
};

struct eulogium_data; /* XXX remove later when struct eulogium gets refactored. */

struct procedure_data *procedures_init(Eldbus_Proxy *proxy);
void procedure_meta_getall(void);
const struct procedure_data *procedure_get(const char *key);
const struct procedure_step *procedure_step_get(const struct procedure_data *proc, const char *key);
void procedure_process_step(struct eulogium_data *eulogium, struct procedure_data *procedure);
Eldbus_Pending *procedure_message(const struct procedure_data *proc, const struct procedure_msg *msg);

struct print_data; /* XXX only here until print_start gets simplified */
Eldbus_Pending *procedure_print_start(const struct print_data *print);
Eldbus_Pending *procedure_print_printer_cleaned(void);
Eldbus_Pending *procedure_print_progress_get(struct procedure_data *procedure);

void *procedure_meta_get(struct procedure_data *procedure);
void procedure_meta_set(struct procedure_data *procedure, void *data);

Eldbus_Pending *procedure_metadata_get(enum procedure_key proc_key);
Eldbus_Pending *procedure_target_set(const enum procedure_key proc_key);

void procedures_shutdown(void);

#endif /* _PROCEDURES_H */
