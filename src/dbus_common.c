/*
 * function and data types for signal handlers
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#include <Elementary.h>

#include "dbus_common.h"
#include "eulogium.h"
#include "eulogium_item_list.h"
#include "procedures.h"

void on_method_generic_ret(void *data EINA_UNUSED, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED)
{
	const char *errname, *errmsg;

	if (eldbus_message_error_get(msg, &errname, &errmsg)) {
		EINA_LOG_ERR("%s %s", errname, errmsg);
		return;
	}
}

void on_method_generic_bool_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED)
{
	const char *errname, *errmsg;
	Eina_Bool ret;

	if (eldbus_message_error_get(msg, &errname, &errmsg)) {
		EINA_LOG_ERR("%s %s", errname, errmsg);
		return;
	}
	if (!eldbus_message_arguments_get(msg, "b", &ret)) {
		EINA_LOG_ERR("Signature mismatch, \"b\".");
		return;
	}
	if (ret)
		EINA_LOG_INFO("Ran %s successfully.", (char *)data);
	else
		EINA_LOG_WARN("%s failed to run.", (char *)data);
}

#define _TECH_ETHERNET "ethernet"
#define _TECH_WIFI "wifi"
void on_method_get_network_info_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED)
{
	struct eulogium_data *eulogium = data; /* TODO replace this by making networks direct via ** */
	struct network_data *networks = NULL;
	const char *errname, *errmsg;
	Eldbus_Message_Iter *array, *array_security, *net_struct;
	char *obj_path, *state, *error, *ssid, *tech, *ipv4, *ipv6, *security;
	uint8_t strength, favorite, immutable, autoconnect;
	uint_fast16_t i = 0;

	printf("get netinfo\n");
	if (eldbus_message_error_get(msg, &errname, &errmsg)) {
		EINA_LOG_ERR("%s %s", errname, errmsg);
		return;
	}
	if (!eldbus_message_arguments_get(msg, "a(ossssybbbssas)", &array)) {
		EINA_LOG_ERR("Message content does not match expected \"a(ossssybbbssas)\" signature. (%s)", eldbus_message_signature_get(msg));
		return;
	}
	while (eldbus_message_iter_get_and_next(array, 'r', &net_struct))
		if (!eldbus_message_iter_arguments_get(net_struct, "ossssybbbssas", &obj_path, &state, &error, &ssid, &tech, &strength, &favorite, &immutable, &autoconnect, &ipv4, &ipv6, &array_security)) {
			EINA_LOG_ERR("Message content does not match expected \"ossssybbbssas\" signature. (%s)", eldbus_message_signature_get(msg));
			break;
		} else {
			networks = realloc(networks, (i + 2) * sizeof(struct network_data));
			if (!networks) {
				EINA_LOG_ERR("Unable to allocate memory.");
			} else {
				size_t obj_pathsize = strlen(obj_path) + 1;

				networks[i].obj_path = malloc(obj_pathsize);
				if (!networks[i].obj_path)
					EINA_LOG_ERR("Unable to allocate memory.");
				else
					strncpy(networks[i].obj_path, obj_path, obj_pathsize);

				networks[i].tech = TECH_UNKNOWN;
				if (!strcmp(tech, _TECH_ETHERNET))
					networks[i].tech = TECH_ETHERNET;
				if (!strcmp(tech, _TECH_WIFI))
					networks[i].tech = TECH_WIFI;

				strncpy(networks[i].ipv4, ipv4, IPV4_MAX_LEN);
				strncpy(networks[i].ipv6, ipv6, IPV6_MAX_LEN);
				strncpy(networks[i].ssid, ssid, SSID_MAX_LEN);
				while (eldbus_message_iter_get_and_next(array_security, 's', &security))
					/* noop */;
			}
			i++;
		}
	networks[i].obj_path = NULL; /* sentinel */
	eulogium->networks = networks;
}

void on_signal_network_info_changed_ret(void *data, const Eldbus_Message *msg)
{
	on_method_get_network_info_ret(data, msg, NULL);
}

void on_method_get_procedure_metadata_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED)
{
	const struct procedure_data *proc = data;
	const char *errname, *errmsg;
	Eldbus_Message_Iter *array;

	if (eldbus_message_error_get(msg, &errname, &errmsg)) {
		EINA_LOG_ERR("%s %s", errname, errmsg); /* XXX do these need to be free'ed? FIXME! YES! They do */
		return;
	}
	if (!eldbus_message_arguments_get(msg, "a{sv}", &array)) {
		EINA_LOG_ERR("Message content does not match expected \"a{sv}\" signature. (%s)", eldbus_message_signature_get(msg)); /* XXX this return value may need to be freed actually!! Yep they do.*/
		return;
	}
	eldbus_message_iter_dict_iterate(array, "sv", proc->parser, proc->meta);
}

/* XXX we are likely not getting a signal, but will deal with this on procedure start and procedure finished. */
void on_signal_procedure_metadata_changed_ret(void *data, const Eldbus_Message *msg)
{
	on_method_get_procedure_metadata_ret(data, msg, NULL);
}

void on_method_is_network_powered_ret(void *data, const Eldbus_Message *msg, Eldbus_Pending *pending EINA_UNUSED)
{
	struct list_item *item = data;
	const char *errname, *errmsg;
	Eina_Bool state;

	if (eldbus_message_error_get(msg, &errname, &errmsg)) {
		EINA_LOG_ERR("%s %s", errname, errmsg);
		return;
	}
	if (!eldbus_message_arguments_get(msg, "b", &state)) {
		EINA_LOG_ERR("Failed to get networking state.");
		return;
	}
	item->state = state;
	if (item->_widget)
		elm_check_state_set(item->_widget, item->state);

	EINA_LOG_INFO("%s is now %d", eldbus_message_member_get(msg), item->state);
}

void on_signal_network_power_changed_ret(void *data, const Eldbus_Message *msg)
{
	on_method_is_network_powered_ret(data, msg, NULL);
}
