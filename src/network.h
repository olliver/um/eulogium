/*
 * functions and data types for networking calls
 *
 * Copyright (c) 2015 Ultimaker B.V.
 * Author: Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 */

#ifndef _NETWORK_H
#define _NETWORK_H

#include <Eina.h>
#include <Eldbus.h>
#include <stdint.h>

#define IPV4_MAX_LEN 16 /* 255.255.255.255\0 */
#define IPV6_MAX_LEN 40 /* ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff\0 */
#define SSID_MAX_LEN 33 /* Section 7.3.2.1 SSID element of IEE802.11-2007 + \0 */

enum network_tech {
	TECH_UNKNOWN,
	TECH_ETHERNET,
	TECH_WIFI,
};

struct network_data {
	char *obj_path;
	enum network_tech tech;
	char ipv4[IPV4_MAX_LEN];
	char ipv6[IPV6_MAX_LEN];
	char ssid[SSID_MAX_LEN];
};

Eldbus_Pending *network_connect_wifi(const struct network_data *network);
void network_init(Eldbus_Proxy *proxy);
void network_shutdown(void);

#endif /* _NETWORK_H */
